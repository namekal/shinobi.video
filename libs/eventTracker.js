var exec = require('child_process').exec
var connectionTester = require('connection-tester')
module.exports = function(config){
    var amplitudeEndpoint = 'https://api.amplitude.com/httpapi?api_key='+config.amplitudeApiToken+'&event='
    var trackEvent = function(username,type,eventData,callback){
        if(!type)type = 'Untitled Event'
        if(!eventData)eventData = null
        if(typeof eventData === 'function'){
            var callback = eventData
            var eventData = null
        }
        var sendData = [
            {
              "user_id": username,
              "event_type": type,
              "event_properties": eventData
            }
        ]
        var eventDataForUrl = encodeURIComponent(JSON.stringify(sendData))
        exec("curl -v '" + amplitudeEndpoint + eventDataForUrl + "'",function(err,data){
            if(err)console.log(err,data)
            if(callback)callback(err,data)
        })
    }
    return trackEvent
}
